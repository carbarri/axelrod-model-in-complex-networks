%%
% cd 'D:\Dropbox\00.Uni\0.Master\2o cuatri\02.SistComp\Trabajo\MatLab'
% cd 'D:\AxelrodData'
% cd 'E:\AxelrodData'
%% NOTAS
% Lo del sueldoooo.
% Running multiple while loops in multiple workers (SPMD): 
% https://es.mathworks.com/help/parallel-computing/execute-simultaneously-on-multiple-data-sets.html#brukbno-4
% ESTE MODELO NO TIENE EN CUENTA LA PRODUCCION CULTURAL!!!!
% Tampoco tiene en cuenta el poder.
% Integrar con como funcionan las neuronas!

%% NETWORK %%
%% Single - WS

clc
clear all

% WS network parameters.  
N=uint16(500);
k=uint16(10);
p=0.9;
H = WattsStrogatz(N,k,p);
A=adjMat(H);                % Adjacency matrix.

% Axelrod parameters. 
F=10;                        % Number of features
q=50;    

[ColorEvol,LinksAct,IterTs]=axelrodFun(A,F,q,5,1000);


%% Single - BA

clc
clear all

% BA network parameters.  
N=500;
M=1;

edgeList=preferential_attachment(N,M);
edgeList=edgeList(:,1:2);

A0=[edgeList ones(length(edgeList),1)];
A=edgeL2adj(A0); 
A=A+A';

H=graph(A);
plot(H,'Layout','force');

%% AXELROD MODEL %%
%% Parameters.

F=2;                        % Number of features
q=3;                        % Range of each feature.

% Axelrod model function.
[ColorEvol,LinksAct,IterTs]=axelrodFun(A,F,q,20,10);
%% Looped - WS

pp=linspace(0,1,20);
for idx1=1:length(pp)
    for idx2=1:10
        clc
        clearvars -except pp idx1 idx2
        
        disp(idx1);
        disp(idx2);

        % WS network parameters.  
        N=uint16(100);
        k=uint16(10);
        p=0.1;
        H = WattsStrogatz(N,k,pp(idx1));
        A=adjMat(H);                % Adjacency matrix.

        % Axelrod parameters. 
        F=5;                        % Number of features
        q=50;                       % Range of each feature.
        
        % Axelrod model function.
        [ColorEvol,LinksAct,IterTs]=axelrodFun(A,F,q,5,1000);

        save(sprintf('Axelrod_v73_N=%d_p=%d_k=%d_F=%d_q=%d_%d.mat',N,pp(idx1),k,F,q,idx2),'ColorEvol','N','p','H','F','q','A','LinksAct','IterTs','-v7.3')
    end
end

%% Looped - BA

NN=linspace(50, 250, 10);
for idx1=1:length(NN)
    for idx2=1:5
        clc
        clearvars -except NN idx1 idx2
        
        disp(idx1);
        disp(idx2);

        % BA network parameters.  
        N=100;
        M=1;

        edgeList=preferential_attachment(NN(idx1),M);
        edgeList=edgeList(:,1:2);

        A0=[edgeList ones(length(edgeList),1)];
        A=edgeL2adj(A0); 
        A=A+A';

        H=graph(A);
        
        % Axelrod parameters. 
        F=10;                        % Number of features
        q=100;                       % Range of each feature.
        
        % Axelrod model function.
        [ColorEvol,LinksAct,IterTs]=axelrodFun(A,F,q,5,1000);

        save(sprintf('Axelrod_BA_v73_N=%d_M=%d_F=%d_q=%d_%d.mat',NN(idx1),M,F,q,idx2),'ColorEvol','N','M','H','F','A','LinksAct','IterTs','-v7.3')
    end
end
%% Load

clear all
clc

load('Axelrod_v73_N=100_p=0_k=10_F=5_q=50_1.mat')

% ColorEvol=squeeze(flip(ColorEvol(:,end:-1000:1),2));
% 
% ColorEvol=squeeze(flip(ColorEvol(:,1,end:-1000:1),3));

%% Ploteamos
%t=1:size(GCC,2);
ColorEvolRGB = hsv2rgb(cat(3,squeeze(ColorEvol), ones(size(ColorEvol,1), size(ColorEvol,2)), ones(size(ColorEvol,1),size(ColorEvol,2))));
%figure(1);
%subplot(121);
pl=plot(H,'EdgeColor','k','EdgeAlpha',0.2,'LineWidth',1,'Layout','force');
pl.MarkerSize=10;
pl.NodeLabel={};
%set(gcf, 'Position',  [100, 100, 1600, 900])
%subplot(122);
for i=1:size(ColorEvolRGB,2)
    pl.NodeColor=squeeze(ColorEvolRGB(:,i,:));
    %plot(1:t(i),GCC(1:i),'.','MarkerSize',20)
    %axis([0 900 0 1])
    drawnow
    
%     disp("ColorEvol")
%     ColorEvol(:,:,i)
%     disp("ColorEvol2")
%     ColorEvol2(:,:,i)
%     disp("Estado")
%     Vstore(:,:,:,i)
end


%% Ploteamos 2
pp=linspace(0,1,20);
qq=([50 134 150 222]);
%pp([1 2 5 20]);
figure(1)
for k1 = 1:length(qq)
    loadedFile=matfile(sprintf('Axelrod_v73_N=100_p=1.000000e-01_k=10_F=10_q=%d_5.mat',qq(k1)));
    ColorEvol=loadedFile.ColorEvol(:,end);
    ColorEvolRGB = hsv2rgb(cat(3,squeeze(ColorEvol), ones(size(ColorEvol,1), size(ColorEvol,2)), ones(size(ColorEvol,1),size(ColorEvol,2))));

    subplot(1,length(qq),k1)
    pl=plot(loadedFile.H,'EdgeColor','k','EdgeAlpha',0.3,'LineWidth',1.7,'Layout','force');
    pl.MarkerSize=10;
    pl.NodeLabel={};
    pl.NodeColor=squeeze(ColorEvolRGB(:,1,:));
    title(sprintf('q=%0.2f',qq(k1)),'fontweight','bold','FontSize', 30);
end
set(gcf, 'Position',  [100, 100, 1000*4, 800])
%% Componente conexa mas grande.
% Guardando las mat. ady. de los subgrafos de cada cultura.

Culturas=unique(ColorEvol(:,end));
NumeroCulturas=length(Culturas);
SubA={};

for i=1:NumeroCulturas
    aux=A;
    % Indices de los elementos que no son la cultura i.
    IdxsDifCult=find(ColorEvol(:,end)~=Culturas(i));
    % Los quitamos y obtenemos la matriz del subgrafo perteneciente a la
    % cultura i.
    aux(IdxsDifCult,:)=[];
    aux(:,IdxsDifCult)=[];
    % Guardamos esta matriz.
    SubA{i} = aux;
    % Vemos cuantos y de que tama�os son los componentes de la cultura i.
    [comps, compsSize]=get_components(aux);
    % Nos quedamos con el de mayor tama�o. Aqu� est�n los componentes m�s 
    % grandes de cada cultura.
    CultMaxCompSize(i)=max(compsSize);
end

GCC=max(CultMaxCompSize);

%% Convertir archivos .mat guardados.
pp=[0.0001 0.0002 0.0004];
for i=1:3
    for j=1:5
        clearvars -except i j pp
        load(sprintf('Axelrod_p=%0.4f_%d.mat',pp(i),j));
        save(sprintf('Axelrod_v73_p=%0.4f_%d.mat', pp(i),j),'ColorEvol','N','k','p','H','F','q','A','LinksAct','IterTs','-v7.3');
    end
    i
end

%% Componente conexa mas grande.
% Haciendo estadistica.
% Sin guardar las mat. ady. de los subgrafos de cada cultura.

%qq=uint16(sort([208 212 213 215 216 217 218 219 221 222 223 225 226 231 237 250 55 60 70 74 78 83 90 95 202 206 210 214 224 228 234 ceil(linspace(2,45,10)) 105 111 122 128 140 145 155 161 173 178 189 195 ceil(linspace(50,200,10)) ceil(linspace(220,300,5))]));
NN=linspace(50, 250, 10);

for k=1:length(NN)
    for j=1:5
        clear Culturas NumeroCulturas aux IdxsDifCult compsSize comps CultMaxCompSize A LastColor

        loadedFile=matfile(sprintf('Axelrod_BA_v73_N=%d_M=1_F=10_q=100_%d.mat',NN(k),j));
        LastColor=loadedFile.ColorEvol(:,end);
        A=loadedFile.A;

        Culturas=unique(LastColor);
        NumeroCulturas=length(Culturas);

        for i=1:NumeroCulturas;
            aux=A;
            % Indices de los elementos que no son la cultura i.
            IdxsDifCult=find(LastColor~=Culturas(i));
            % Los quitamos y obtenemos la matriz del subgrafo perteneciente a la
            % cultura i.
            aux(IdxsDifCult,:)=[];
            aux(:,IdxsDifCult)=[];
            % Vemos cuantos y de que tama�os son los componentes de la cultura i.
            [comps, compsSize]=get_components(aux);
            % Nos quedamos con el de mayor tama�o. Aqu� est�n los componentes m�s 
            % grandes de cada cultura.
            CultMaxCompSize(i)=max(compsSize);
        end
        GCCsizes(k,j)=max(CultMaxCompSize);
    end
    k
end


% tururu=matfile('Apartado1_GCCsizes.mat')
% meanGCCsize = (sum(GCCsizes,2)/5)/500;
% meanGCCsize2= (sum(tururu.GCCsizes,2)/5)/500;
% meanGCCsizeTOT =[meanGCCsize;meanGCCsize2];
% ppTOT=[pp tururu.pp];

meanGCCsize = (sum(GCCsizes(1:7,:),2)/5)'./NN(1:7);
GCCstd = std(GCCsizes(1:7,:)')./NN(1:7);


%% Ploteamos 3
figure(1)
hold on
errorbar(NN(1:7),meanGCCsize,GCCstd,'k.','MarkerSize',40,'LineWidth',1.5);
pltot=scatter([NN(1:7) NN(1:7) NN(1:7) NN(1:7) NN(1:7)],reshape(GCCsizes,[1 length(GCCsizes)*5])/100,80,'k','filled');
hold off
% text(25,0.7,' q_c ~ 115 ','fontweight','bold','FontSize', 30)
set(gca,'LineWidth',2)
set(gca,'FontSize',24)
set(gcf, 'Position',  [100, 100, 1280, 720])
title('Barabasi-Albert ( N=100, q=100, F=10 )','FontSize', 30)
xlabel('M','fontweight','bold', 'FontSize', 34);
ylabel('GCC/N','fontweight','bold', 'FontSize', 30)
alpha(pltot,.2 )
% semilogx(kk,meanGCCsize,'o');
%%

save('Apartado1_GCCsizes.mat','p','GCCsizes');
%%
load('Apartado1_GCCsizes.mat');
%%
p
GCCsizes

%%
semilogx(p(2:end),GCCsizes(:,2:end)/500,'.','MarkerSize',20)


%% Scale-Free

%% Evolucion de la componente gigante.

clear Culturas NumeroCulturas aux IdxsDifCult compsSize comps CultMaxCompSize GCC
for j=1:size(ColorEvol,2)
    Culturas=unique(ColorEvol(:,j));
    NumeroCulturas(j)=length(Culturas);
    for i=1:NumeroCulturas(j)
        aux=A;
        % Indices de los elementos que no son la cultura i.
        IdxsDifCult=find(ColorEvol(:,j)~=Culturas(i));
        % Los quitamos y obtenemos la matriz del subgrafo perteneciente a la
        % cultura i.
        aux(IdxsDifCult,:)=[];
        aux(:,IdxsDifCult)=[];
        % Vemos cuantos y de que tama�os son los componentes de la cultura i.
        [comps, compsSize]=get_components(aux);
        % Nos quedamos con el de mayor tama�o. Aqu� est�n los componentes m�s 
        % grandes de cada cultura.
        CultMaxCompSize(i)=max(compsSize);
    end
    GCC(j)=max(CultMaxCompSize);
    j
end
scatter(1:size(ColorEvol,2),GCC/double(N))

%%
%%%%%%%%%%%%%%%%%%
% Exportamos GIF %
%%%%%%%%%%%%%%%%%%

h = figure;
axis tight manual % this ensures that getframe() returns a consistent size
filename = 'axelrodTest.gif';

for n =1:30:size(ColorEvol,3)
    pl=plot(H,'EdgeColor','k','EdgeAlpha',0.7);
    pl.MarkerSize=7;
    pl.NodeLabel={};
    pl.NodeColor=ColorEvol(:,:,n);
    drawnow 
      % Capture the plot as an image 
      frame = getframe(h); 
      im = frame2im(frame); 
      [imind,cm] = rgb2ind(im,256); 
      % Write to the GIF File 
      if n == 1 
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
      else 
          imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0); 
      end
      n
end
%%
filename = 'percolationBA.gif';
h = figure;
axis tight manual % this ensures that getframe() returns a consistent size
t=1:size(GCC,2);
ColorEvolRGB = hsv2rgb(cat(3,squeeze(ColorEvol), ones(size(ColorEvol,1), size(ColorEvol,2)), ones(size(ColorEvol,1),size(ColorEvol,2))));
subplot(121);
pl=plot(H,'EdgeColor','k','EdgeAlpha',0.2,'LineWidth',1,'Layout','force');
pl.MarkerSize=8;
pl.NodeLabel={};
set(gcf, 'Position',  [100, 100, 1600, 900])
subplot(122);
for i=9:10:size(ColorEvolRGB,2)
    pl.NodeColor=squeeze(ColorEvolRGB(:,i,:));
    plot(1:t(i),GCC(1:i)/500,'.','MarkerSize',20)
    axis([0 1000 0 1])
    title('Percolaci�n: Modelo de Axelrod - Red de Barabasi-Albert ( N=500, M=1, F=10 )                                                                          ','FontSize', 25)
    xlabel('t','fontweight','bold', 'FontSize', 34);
    ylabel('GCC/N','fontweight','bold', 'FontSize', 30)
    drawnow
    
      % Capture the plot as an image 
      frame = getframe(h); 
      im = frame2im(frame); 
      [imind,cm] = rgb2ind(im,256); 
      % Write to the GIF File 
      if i == 9 
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
      else 
          imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0); 
      end
      i
end
%%        
%%%%%%%%%%%% 
% Trastero %
%%%%%%%%%%%%

% State matrix 
v=randi(q,N,F);
Vneo=permute(v.*ones(N,F,N),[1,3,2]);
Vpermneo=permute(v.*ones(N,F,N),[3,1,2]);
stateneo =numLinksTot;
c=1;
ColorEvol(:,:,c)=cultures2colorsHue(q,reshape(Vneo(:,1,:),[N,F]));
% Vstore(:,:,:,c)=V;
% Main loop

stateneo
Wneo=1-(Vneo-Vpermneo==0);               % t~0.31s
Oneo=A.*Wneo;
Lneo=(sum(Wneo,3).*A)/F;
tic
while stateneo>ceil(numLinksTot/20);
    
    % Parece que no estamos teniendo en cuenta que hay mayor probabilidad
    % de contagio si el solapamiento entre nodos es mayor.

    % Change                        % t~0.11s

    [filneo,colneo]=find(Lneo);      % Active links
    
    stateneo=length(filneo)/2;    % Num de links activos.
    LinksAct(c)=stateneo;
    
    if mod(c,1000)==0
        clc
        oneIterT(c/1000)=toc;
        stateneo
        sum(sum(Lneo))
        c
        tic
    end
    
    wneo=randi(length(filneo));   % Chose an locate one of them
    ineo=filneo(wneo);
    jneo=colneo(wneo);
    channeo=datasample(find(Oneo(jneo,ineo,:)),1);
    Vneo(ineo,:,channeo)=Vneo(jneo,:,channeo);
    Vpermneo(:,ineo,channeo)=Vpermneo(:,jneo,channeo);
    
    c=c+1;
    
    % Guardamos estado.
    %Vstore(:,:,:,c)=V;
    
    ColorEvol(:,:,c)=cultures2colorsHue(q,reshape(Vneo(:,1,:),[N,F]));

    % elemento a elemento
    
    Wneo(:,ineo,channeo)=1-((Vneo(:,1,channeo)-Vneo(jneo,1,channeo))==0);
    Wneo(ineo,:,channeo)=Wneo(:,ineo,channeo)';
    
    Oneo(ineo,:,channeo)=A(ineo,:).*Wneo(ineo,:,channeo);
    Oneo(:,ineo,channeo)=Oneo(ineo,:,channeo)';
    
    Lneo(ineo,:)=(sum(Wneo(ineo,:,:),3).*A(ineo,:))/F;
    Lneo(:,ineo)=Lneo(ineo,:)';
end