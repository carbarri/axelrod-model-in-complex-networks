function colors = cultures2colors(q,arr)
%generar un numero para cada vector de estado.
F=size(arr,2);
N=size(arr,1);
colors = hsv2rgb([rescale((arr-1)*(q.^linspace(F-1,0,F))'+1,'InputMin',1,'InputMax',q^F) ones(N,1) ones(N,1)]);
end

