function [CULT,LINKS,time] = axelrodFun(A,F,q,u,iterPrint)
% Calculo de la evolucion del modelo de Axelrod en una red compleja.
%   A: matriz de adyacencia de la red.
%   F: numero de caracteristicas culturales.
%   q: rango de valores de cada caracteristica cultural.
%   u: condicion de estacionariedad (% de links activos).
%   iterPrint: cantidad de iteraciones 
%
%
%   CULT: contiene la evolucion de la cultura de cada nodo.
%   LINKS: contiene la evolucion del numero de Links activos.
%   time: contiene tiempo de ejecucion cada iterPrint iteraciones.

% State matrix 
N=length(A);
v=randi(q,N,F);
Vneo=permute(v.*ones(N,F,N),[1,3,2]);
Vpermneo=permute(v.*ones(N,F,N),[3,1,2]);
numLinksTot=length(find(A))/2;
stateneo=numLinksTot;

c=1;
ColorEvol(:,:,c)=cultures2colorsHue(q,reshape(Vneo(:,1,:),[N,F]));

% Main loop
Wneo=1-(Vneo-Vpermneo==0);          % t~0.31s
Oneo=A.*Wneo;
Lneo=(sum(Wneo,3).*A)/F;
[filneo,colneo]=find(Lneo);     % Active links
tic
while c<2000 %&& stateneo>ceil(u*numLinksTot/100)

    % Parece que no estamos teniendo en cuenta que hay mayor probabilidad
    % de contagio si el solapamiento entre nodos es mayor.

    % Change                        % t~0.11s
       
    wneo=randi(length(filneo));     % Chose an locate one of them
    ineo=filneo(wneo);
    jneo=colneo(wneo);
    channeo=datasample(find(Oneo(jneo,ineo,:)),1);
    Vneo(ineo,:,channeo)=Vneo(jneo,:,channeo);
    Vpermneo(:,ineo,channeo)=Vpermneo(:,jneo,channeo);
    
    c=c+1;
    
    % Guardamos estado.
    
    ColorEvol(:,:,c)=cultures2colorsHue(q,reshape(Vneo(:,1,:),[N,F]));

    % elemento a elemento
    
    Wneo(:,ineo,channeo)=1-((Vneo(:,1,channeo)-Vneo(jneo,1,channeo))==0);
    Wneo(ineo,:,channeo)=Wneo(:,ineo,channeo)';
    
    Oneo(ineo,:,channeo)=A(ineo,:).*Wneo(ineo,:,channeo);
    Oneo(:,ineo,channeo)=Oneo(ineo,:,channeo)';
    
    Lneo(ineo,:)=(sum(Wneo(ineo,:,:),3).*A(ineo,:))/F;
    Lneo(:,ineo)=Lneo(ineo,:)';
    
    [filneo,colneo]=find(Lneo);     % Active links
    
    stateneo=length(filneo)/2;      % Num de links activos.
    LinksAct(c)=stateneo;
    
    if mod(c,iterPrint)==0
        oneIterT(c/iterPrint)=toc;
        fprintf('Iteration: %d.\n',c);
        fprintf('Active links (weighted): %d (%0.2f).\n',stateneo,sum(sum(Lneo)));
        fprintf('Execution time (%d iterations): %0.4f.\n',iterPrint,oneIterT(c/iterPrint));
        tic
    end
end
CULT=squeeze(ColorEvol);
LINKS=LinksAct;
time=oneIterT;
end

