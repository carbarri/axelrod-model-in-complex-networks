function connect = scale_free_add(G, n, m, a = 0, mode = "strict")
	ki = sum(G(1:n - 1,1:n-1));
	sumkj = sum(ki);
	pi = (a + ki)/sumkj;

	connect = zeros (1, n - 1);

	while (sum(connect) < m)
 		connect |= (pi >= rand (1,n - 1));
	endwhile

	if (strcmp (mode, "strict") && sum(connect) > m)
		perm = randperm (n - 1);
		c = connect(perm);
		added = 0;
		% a binary search would make this faster
                % or perhaps using sort
		for i = 1:columns(c)
			if (added < m)
				added += c(i);
			else
				connect(perm(i)) = 0;
            end
        end
    end 
end
