function color = culture2color(q,arr)
%generar un numero para cada vector de estado.
color = dot((arr-1),q.^linspace(length(arr)-1,0,length(arr)))+1;
end

