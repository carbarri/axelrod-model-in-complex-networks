# Axelrod model in complex networks

Matlab code that performs Axelrod dissemination of culture model on complex networks. We study the evolution of the GCC (Giant Connected Component) using different number of cultural features on Watts-Strogatz and scale-free (Barabasi-Albert) networks.

The color represent the particular likes of each node (person).

![percolationBA](/doc/percolation.gif)*Evolution of the GCC in a Watts-Strogatz network.*

![percolationBA](/doc/percolationBA.gif)*Evolution of the GCC in a Barabasi-Albert network.*
