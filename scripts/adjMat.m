function [A] = adjMat(H)
% Matriz de adyacencia.
% H: class graph.
    A0=table2array(H.Edges);
    A0=[A0 ones(length(A0),1)];
    A=edgeL2adj(A0); 
    A=A+A';
end

